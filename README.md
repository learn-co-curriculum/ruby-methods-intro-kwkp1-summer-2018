# You will be able to

1. Learn how to find Ruby's **built-in methods in the Ruby documentation**
2. Write your own **methods** in Ruby
2. Use **interactive ruby (IRB)** to test out your code

# Methods

**Methods** are the secret sauce to writing code in Ruby. **A method is a reusable chunk of code written to perform a specific task in a program**, like reverse a string or add 10 to a number.
